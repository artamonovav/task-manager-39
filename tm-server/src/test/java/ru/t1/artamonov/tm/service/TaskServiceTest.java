package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.model.Task;

import static ru.t1.artamonov.tm.constant.TaskTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
import static ru.t1.artamonov.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    ITaskService taskService = new TaskService(connectionService);

    @Test
    public void add() {
        taskService.add(USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        taskService.remove(USER1_TASK1);
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
    }

    @Test
    public void addByUserId() {
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        taskService.remove(USER1_TASK1);
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
    }

    @Test
    public void clearByUserId() {
        taskService.clear(USER2.getId());
        taskService.clear(USER1.getId());
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(3, taskService.getSize(USER1.getId()));
        taskService.clear(USER2.getId());
        Assert.assertEquals(0, taskService.getSize(USER2.getId()));
        taskService.clear(USER1.getId());
        Assert.assertEquals(0, taskService.getSize(USER1.getId()));
    }

    @Test
    public void findAllByUserId() {
        taskService.add(TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll(USER1.getId()).size());
        taskService.removeAll(TASK_LIST);
    }

    @Test
    public void findOneByIdByUserId() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
        taskService.remove(USER1_TASK1);
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.remove(USER1.getId(), USER1_TASK1).getId());
        taskService.remove(USER1_TASK1);
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1.getId(), taskService.removeById(USER1.getId(), USER1_TASK1.getId()).getId());
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertTrue(taskService.existsById(USER2_TASK1.getId()));
        taskService.remove(USER1_TASK1);
        taskService.remove(USER2_TASK1);
    }

    @Test
    public void existsByIdByUserId() {
        taskService.add(USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
        taskService.remove(USER1_TASK1);
    }

    @Test
    public void changeTaskStatusById() {
        taskService.add(USER1_TASK1);
        @NotNull Task task = taskService.findOneById(USER1_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
        @NotNull Task task1 = taskService.findOneById(USER1_TASK1.getId());
        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
        taskService.remove(USER1_TASK1);
    }

    @Test
    public void createTaskName() {
        @NotNull Task task = taskService.create(USER1.getId(), "test_task");
        @NotNull Task task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals("test_task", task1.getName());
        Assert.assertEquals(USER1.getId(), task1.getUserId());
        taskService.remove(task);
    }

    @Test
    public void createTaskNameDescription() {
        @NotNull Task task = taskService.create(USER1.getId(), "test_task", "test_description");
        @NotNull Task task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals("test_task", task1.getName());
        Assert.assertEquals("test_description", task1.getDescription());
        Assert.assertEquals(USER1.getId(), task1.getUserId());
        taskService.remove(task);
    }

    @Test
    public void updateById() {
        @NotNull Task task = taskService.create(USER1.getId(), "test_task", "test_description");
        taskService.updateById(USER1.getId(), task.getId(), "new name", "new description");
        @NotNull Task task1 = taskService.findOneById(task.getId());
        Assert.assertEquals("new name", task1.getName());
        Assert.assertEquals("new description", task1.getDescription());
    }

}
