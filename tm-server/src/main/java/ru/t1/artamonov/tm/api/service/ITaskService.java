package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task add(@Nullable Task model);

    @NotNull
    Task add(@Nullable String userId, @Nullable Task model);

    @NotNull
    Collection<Task> add(@NotNull Collection<Task> models);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<Task> findAll();

    @Nullable
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<Task> findAll(@Nullable Comparator<Task> comparator);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator<Task> comparator);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Task findOneById(@Nullable String id);

    @Nullable
    Task findOneById(@Nullable String userId, @Nullable String id);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    Task remove(@Nullable Task model);

    @NotNull
    Task remove(@Nullable String userId, @Nullable Task model);

    void removeAll(@Nullable Collection<Task> collection);

    @NotNull
    Task removeById(@Nullable String id);

    @NotNull
    Task removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Collection<Task> set(@NotNull Collection<Task> models);

    @NotNull
    Task update(@NotNull Task model);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
