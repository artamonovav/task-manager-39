package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@Nullable Session model);

    @NotNull
    Session add(@Nullable String userId, @Nullable Session model);

    @NotNull
    List<Session> add(@NotNull List<Session> models);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String id);

    @Nullable
    List<Session> findAll();

    @Nullable
    Session findOneById(@Nullable String id);

    @Nullable
    Session findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Session remove(@Nullable Session model);

    @NotNull
    Session removeById(@Nullable String id);

    @NotNull
    Session removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    Session update(@Nullable Session model);

}
